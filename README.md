# Introduction

This is the Collosal Cave Adventure Docker Container primarily created for use as a test gateway for the Telstar Videotex Service. Please see the johnnewcombe/telstar Docker image for more details of the Telstar Service.

# Dockerfile

    FROM ubuntu
    LABEL maintainer="John Newcombe, https://glasstty.com"
    ENV REFRESHED_AT 2020-11-29
    ENV DEBIAN_FRONTEND="noninteractive" TZ="Europe/London"
    
    RUN apt-get -y update \
            && apt-get -y install \
            git \
            nano \
            python3-tornado \
            python3-pip \
            && pip3 install adventure \
            && rm -rf /var/lib/apt/lists/*
    
    # Add the startup script
    COPY adventure_server.py advent40.dat /opt/adventureserver/
    
    # copy the package
    WORKDIR /opt/adventureserver
    ENTRYPOINT ["python3", "adventure_server.py"]

# Docker Compose

    version: "3.8"
    services:
      adventure:
        container_name: "adventure-server"
        restart: always
        build: .
        image: "johnnewcombe/adventure"
        command: "6505"
        networks:
          telstar-network:
    networks:
      telstar-network:
        name: telstar-network