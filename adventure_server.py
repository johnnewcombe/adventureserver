import logging
import struct
import traceback
import argparse
import os
import re

from adventure import load_advent_dat
from adventure.game import Game

from tornado import gen
from tornado.ioloop import IOLoop
from tornado.iostream import StreamClosedError
from tornado.options import options as tornado_options
from tornado.tcpserver import TCPServer


parser = argparse.ArgumentParser()
parser.add_argument("port", type=int, help="port to listen on")
opts = parser.parse_args()


logger = logging.getLogger(os.path.basename(__file__))
logger.setLevel(logging.INFO)

# Cache this struct definition; important optimization.
int_struct = struct.Struct("<i")
_UNPACK_INT = int_struct.unpack
_PACK_INT = int_struct.pack

loop = IOLoop.current()


class TornadoServer(TCPServer):

    def __init__(self):

        TCPServer.__init__(self)
        self.current_frame = None

    def load_advent_dat(self, data):

        """
        This method is used in place of the load_advent_dat method defined in data.py of the Adventure package.
        The method allows a alternative dat file to be specified without affecting the package.
        """
        import os
        datapath = os.path.join(os.path.dirname(__file__), 'advent40.dat')
        from adventure.data import parse

        with open(datapath, 'r', encoding='ascii') as datafile:
            parse(data, datafile)

    @gen.coroutine
    def handle_stream(self, stream, address):

        logging.info("[adventure] Connection from client address {0}.".format(address))

        try:
            line = ''
            game = Game()
            self.load_advent_dat(game)


            game.start()
            stream.write(game.output.encode('utf-8'))

            while True:

                char = yield stream.read_bytes(1)

                # tidy up any Prestel type CRs
                if char == b'\x5f':
                    char = b'\r'

                if ord(char) < 128:
                    line += char.decode()

                print('[adventure] char received = {0} [{1}]'.format(char, ord(char)))

                if char == b'\r':

                    command = line.lower()
                    print('[adventure] command = {0}'.format(command))

                    if command.startswith('save') : # disable 'SAVE'
                        command = 'evas'

                    words = re.findall(r'\w+', command)

                    if words:
                        response = game.do_command(words)
                        logging.info('[adventure] command response = {0}'.format(response))
                        stream.write(response.encode('utf-8'))
                    line = ''

                else:
                    # no echo on this service is better for telnet connections and reduces bandwidth
                    pass

                if game.is_done:
                    # TODO: Investigate how to signal a clean exit back to any calling system, e.g. TELSTAR.
                    stream.write(b"\r\nDisconnected...")
                    stream.close()

        except StreamClosedError as ex:
            logging.info("[adventure] Client address {0} disconnected.".format(address))

        except Exception as ex:
            logger.error("[adventure] {0} Exception: {1} Message: {2}".format(address, type(ex), str(ex)))
            logger.error(traceback.format_exc())


if __name__ == '__main__':

    server = TornadoServer()
    server.listen(opts.port)
    logging.info("[adventure] Starting server.")
    loop.start()