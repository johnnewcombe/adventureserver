#!/usr/bin/env bash

if [ -z "$1" ]
  then
    echo "No <port> argument specified."
    exit 1
fi

echo Shutting Down Services
sudo systemctl status adventure-$1.service
sudo systemctl stop adventure-$1.service
sudo systemctl disable adventure-$1.service
sudo systemctl status adventure-$1.service

echo Removing old files
sudo rm -v /opt/adventure/$1/*.pyc
sudo rm -v /opt/adventure/$1/*.py
sudo rm -vrdf /opt/python-adventure

echo Copying New Files
sudo touch *
sudo cp -v ./*.py /opt/adventure/$1/
sudo cp -v ./*.dat /opt/adventure/$1/
sudo cp -vr ../python-adventure/adventure /opt/adventure/$1/
sudo cp -v ./*.service /etc/systemd/system/

echo Restarting Services
sudo systemctl enable adventure-$1.service
sudo systemctl start adventure-$1.service
sudo systemctl status adventure-$1.service

